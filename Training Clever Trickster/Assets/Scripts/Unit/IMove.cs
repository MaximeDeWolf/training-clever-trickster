using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMove
{
    void GoTo(Vector3 destination);
}

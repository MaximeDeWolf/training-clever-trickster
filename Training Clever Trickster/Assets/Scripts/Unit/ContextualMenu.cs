using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(RightClickUnitEvent))]
[RequireComponent(typeof(CancelRightClickUnit))]
public class ContextualMenu : MonoBehaviour
{
    [Tooltip("The actions to render in the contextual menu")]
    [SerializeField]
    private List<GameObject> contextualActions = new List<GameObject>();
    private bool isContextualPanelDisplayed = false;

    public GameObject ContextualPanel { get; set; }


    private List<ContextualAction> actions = new List<ContextualAction>();

    private void Start()
    {
        foreach(GameObject gameObject in contextualActions)
        {
            GameObject newActionObject = Instantiate(gameObject, transform);
            ContextualAction action = newActionObject.GetComponent<ContextualAction>();
            if(action)
            {
                action.Init(this.gameObject);
                actions.Add(action);
            }
        }
    }

    private void OnEnable()
    {
        GetComponent<RightClickUnitEvent>().RightClickUnitReceived += PopMenu;
        GetComponent<CancelRightClickUnit>().CancelRightClickUnitReceived += DepopMenu;
    }

    private void OnDisable()
    {
        GetComponent<RightClickUnitEvent>().RightClickUnitReceived -= PopMenu;
        GetComponent<CancelRightClickUnit>().CancelRightClickUnitReceived -= DepopMenu;
    }

    private void PopMenu()
    {
        ContextualPanel.GetComponent<ContextualMenuCreator>().BuildMenu(actions);
        ContextualPanel.GetComponent<Image>().enabled = true;
        isContextualPanelDisplayed = true;
    }

    private void DepopMenu()
    {
        // Prevent other building to clear the menu
        if (isContextualPanelDisplayed)
        {
            ContextualPanel.GetComponent<Image>().enabled = false;
            ContextualPanel.GetComponent<ContextualMenuCreator>().ClearMenu();
            isContextualPanelDisplayed = false;
        }

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementEvent))]
public class ClickPrinter : MonoBehaviour
{


        public void Print(Vector3 destination)
        {
            Debug.Log(destination);
        }

        private void OnEnable()
        {
            GetComponent<MovementEvent>().DestinationReceived += Print;
        }

        private void OnDisable()
        {
            GetComponent<MovementEvent>().DestinationReceived -= Print;
        }
}

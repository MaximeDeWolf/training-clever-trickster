using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RightClickUnitEvent))]
[RequireComponent(typeof(CancelRightClickUnit))]
public class RightClickUnit : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            // Check is pointer is over UI
            if (EventSystem.current.IsPointerOverGameObject())
            {
                GetComponent<CancelRightClickUnit>().SendCancelRightClickSignal();
                return;
            }

            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);

            if (Physics.Raycast(ray, out hit, Camera.main.transform.position.y + 10))
            {
                if (hit.collider.gameObject == gameObject)
                {
                    GetComponent<RightClickUnitEvent>().SendRightClickSignal();
                    return;
                }
            }

            GetComponent<CancelRightClickUnit>().SendCancelRightClickSignal();
        }
    }
}

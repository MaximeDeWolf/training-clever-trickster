using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCreationHandler : MonoBehaviour
{
    [Tooltip("The panel object that represents the contextual menu")]
    [SerializeField]
    private GameObject contextualPanel;

    private BuildingCreationRequestEvent creationEvent;

    private void Awake()
    {
        creationEvent = BuildingCreationRequest.GetEvent();
    }

    private void OnEnable()
    {
        creationEvent.BuildingCreationRequestReceived += SpawnBuilding;
    }

    private void OnDisable()
    {
        creationEvent.BuildingCreationRequestReceived -= SpawnBuilding;
    }

    private void SpawnBuilding(GameObject prefab, Vector3 position)
    {
        GameObject newObject = Instantiate(prefab, position, new Quaternion());
        ContextualMenu contextualMenu = newObject.GetComponent<ContextualMenu>();
        if (contextualMenu)
            contextualMenu.ContextualPanel = contextualPanel;
    }
}

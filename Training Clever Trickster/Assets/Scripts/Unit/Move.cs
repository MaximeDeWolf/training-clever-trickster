using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementEvent))]
public class Move : MonoBehaviour, IMove
{

    public void GoTo(Vector3 destination)
    {
        Vector3 destination3D = new Vector3(destination.x, 0, destination.z);
        transform.position = destination3D;
    }

    private void OnEnable()
    {
        GetComponent<MovementEvent>().DestinationReceived += GoTo;
    }

    private void OnDisable()
    {
        GetComponent<MovementEvent>().DestinationReceived -= GoTo;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingCreationPublisher : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [Tooltip("The prefab to spawn at the drop position")]
    [SerializeField]
    private GameObject prefab;

    private BuildingCreationRequestEvent creationEvent;

    private void Awake()
    {
        creationEvent = BuildingCreationRequest.GetEvent();
    }

    // IDragHandler interface
    public void OnDrag(PointerEventData eventData)
    {
        return;
    }

    // IEndDragHandler interface
    public void OnEndDrag(PointerEventData eventData)
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        creationEvent.SendBuildingCreationRequest(prefab, worldPosition);
    }
}

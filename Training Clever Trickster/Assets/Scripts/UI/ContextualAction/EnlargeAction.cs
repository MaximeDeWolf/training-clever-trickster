using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnlargeAction : ContextualAction
{

    public override void Init(GameObject gameObject)
    {
        GameObjectCaller = gameObject;
        ActionName = "Enlarge";
    }

    public override void DoAction()
    {
        GameObjectCaller.transform.localScale += new Vector3(1, 0, 1);
    }
}

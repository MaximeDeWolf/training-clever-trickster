using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowAction : ContextualAction
{

    public override void Init(GameObject gameObject)
    {
        GameObjectCaller = gameObject;
        ActionName = "Grow";
    }

    public override void DoAction()
    {
        GameObjectCaller.transform.localScale += new Vector3(0, 1, 0);
    }
}

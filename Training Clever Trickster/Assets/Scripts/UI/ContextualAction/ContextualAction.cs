using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ContextualAction: MonoBehaviour
{
    public GameObject GameObjectCaller { get; protected set; }
    public string ActionName { get; protected set; }

    public abstract void DoAction();
    public abstract void Init(GameObject gameObject);
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ContextualMenuCreator : MonoBehaviour
{
    [Tooltip("The prefab of the contextual button")]
    [SerializeField]
    private GameObject contextualButtonPrefab;

    public void BuildMenu(List<ContextualAction> contextualActions)
    {
        foreach(ContextualAction action in contextualActions)
        {
            GameObject newButton = Instantiate(contextualButtonPrefab, transform);
            TMPro.TextMeshProUGUI text = newButton.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            if (text) {
                text.SetText(action.ActionName);
            }
            newButton.GetComponent<Button>().onClick.AddListener(() => action.DoAction());
        }
    }

    public void ClearMenu()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            Destroy(child.gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOnGameObject : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            CenterCameraOnThisObject();
        }
    }

    private void CenterCameraOnThisObject()
    {
        Vector3 newPosition = transform.position;
        newPosition.y = Camera.main.transform.position.y;
        Camera.main.transform.position = newPosition;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithDrag : MonoBehaviour
{
    [Tooltip("The sensitivity of the camera movement (0 means no movement)")]
    [Min(0)]
    [SerializeField]
    private float sensitivity = 2f;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Fire3") >= 0.5f)
        {
            float inputX = Input.GetAxis("Mouse X");
            float inputY = Input.GetAxis("Mouse Y");
            // Substract the movement because we need a movement in the other direction of the mouse to simulate the click and drag
            Camera.main.transform.position -= sensitivity * new Vector3(inputX, 0f, inputY);
        }
    }
}

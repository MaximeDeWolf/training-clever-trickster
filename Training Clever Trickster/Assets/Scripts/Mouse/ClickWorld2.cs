using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementEvent))]
public class ClickWorld2 : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            GetComponent<MovementEvent>().PropagateDestination(worldPosition);
        }
    }
}

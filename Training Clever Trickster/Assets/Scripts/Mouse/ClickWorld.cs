using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


[RequireComponent(typeof(MovementEvent))]
public class ClickWorld : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // Check is pointer is over UI
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y);
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);

            if ( !Physics.Raycast(ray, out hit, Camera.main.transform.position.y + 10) ){
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
                GetComponent<MovementEvent>().PropagateDestination(worldPosition);
            }   
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementEvent : MonoBehaviour
{
    public delegate void MovementHandler(Vector3 destination);

    public event MovementHandler DestinationReceived;

    public void PropagateDestination(Vector3 destination) => DestinationReceived?.Invoke(destination);
}

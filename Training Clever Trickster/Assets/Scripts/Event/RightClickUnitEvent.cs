using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightClickUnitEvent: MonoBehaviour
{

    public delegate void RightClickUnittHandler();

    public event RightClickUnittHandler RightClickUnitReceived;

    public void SendRightClickSignal() => RightClickUnitReceived?.Invoke();
}

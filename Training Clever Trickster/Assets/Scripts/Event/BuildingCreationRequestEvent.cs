using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCreationRequest
{
    private static BuildingCreationRequestEvent eventInstance;

    public static BuildingCreationRequestEvent GetEvent()
    {
        if (eventInstance == null)
            eventInstance = new BuildingCreationRequestEvent();
        return eventInstance;
    }
}

public class BuildingCreationRequestEvent
{
    public delegate void BuildingCreationRequestHandler(GameObject prefab, Vector3 newBuildingPosition);

    public event BuildingCreationRequestHandler BuildingCreationRequestReceived;

    public void SendBuildingCreationRequest(GameObject prefab, Vector3 newBuildingPosition) => BuildingCreationRequestReceived?.Invoke(prefab, newBuildingPosition);
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelRightClickUnit : MonoBehaviour
{
    public delegate void CancelRightClickUnittHandler();

    public event CancelRightClickUnittHandler CancelRightClickUnitReceived;

    public void SendCancelRightClickSignal() => CancelRightClickUnitReceived?.Invoke();
}
